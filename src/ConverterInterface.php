<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Converter;

use Paneric\Interfaces\Serializer\SerializerInterface;

interface ConverterInterface extends SerializerInterface
{
    public function convert(): array;
}
